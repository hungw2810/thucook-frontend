import { Storage, STORAGE_KEYS } from "../../utility/storage";
import ACTION_TYPES from '../actionTypes'

export const loginAC = data => {
   return dispatch => {
      dispatch({
         type: ACTION_TYPES.LOGIN,
         data
      })
      // ** Add to user, accessToken & refreshToken to localStorage
      Storage.setItem(STORAGE_KEYS.userData, data.user)
      Storage.setItem(STORAGE_KEYS.token, data.token)
   }
}