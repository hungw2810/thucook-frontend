export const KeyBoardEnum = {
  Escape: "Escape",
  Tab: "Tab",
  Enter: "Enter",
};

export const CityEnum = {
  HaNoi: 24,
  TPHoChiMinh: 58,
  AnGiang: 1,
  BaRiaVungTau: 2,
  BacGiang: 3,
  BacKan: 4,
  BacLieu: 5,
  BacNinh: 6,
  BenTre: 7,
  BinhDinh: 8,
  BinhDuong: 9,
  BinhPhuoc: 10,
  BinhThuan: 11,
  CaMau: 12,
  CanTho: 13,
  CaoBang: 14,
  DaNang: 15,
  DakLak: 16,
  DakNong: 17,
  DienBien: 18,
  DongNai: 19,
  DongThap: 20,
  GiaLai: 21,
  HaGiang: 22,
  HaNam: 23,
  HaTinh: 25,
  HaiDuong: 26,
  HaiPhong: 27,
  HauGiang: 28,
  HoaBinh: 29,
  HungYen: 30,
  KhanhHoa: 31,
  KienGiang: 32,
  KonTum: 33,
  LaiChau: 34,
  LamDong: 35,
  LangSon: 36,
  LaoCai: 37,
  LongAn: 38,
  NamDinh: 39,
  NgheAn: 40,
  NinhBinh: 41,
  NinhThuan: 42,
  PhuTho: 43,
  PhuYen: 44,
  QuangBinh: 45,
  QuangNam: 46,
  QuangNgai: 47,
  QuangNinh: 48,
  QuangTri: 49,
  SocTrang: 50,
  SonLa: 51,
  TayNinh: 52,
  ThaiBinh: 53,
  ThaiNguyen: 54,
  ThanhHoa: 55,
  ThuaThienHue: 56,
  TienGiang: 57,
  TraVinh: 59,
  TuyenQuang: 60,
  VinhLong: 61,
  VinhPhuc: 62,
  YenBai: 63,
};

export const MedicineUnitTypeEnum = {
  Vỉ: 1,
  Hộp: 2,
  Gói: 3,
  Tuýp: 4,
  Chai: 5,
};

export const MedicineUnitTypeDisplayConfig = {
  [MedicineUnitTypeEnum.Vỉ]: { title: "Vỉ" },
  [MedicineUnitTypeEnum.Hộp]: { title: "Hộp" },
  [MedicineUnitTypeEnum.Gói]: { title: "Gói" },
  [MedicineUnitTypeEnum.Tuýp]: { title: "Tuýp" },
  [MedicineUnitTypeEnum.Chai]: { title: "Chai" },
};

export const GenderEnum = {
  Male: 0,
  Female: 1,
};

export const GenderDisplayConfig = {
  [GenderEnum.Male]: { title: "Nam" },
  [GenderEnum.Female]: { title: "Nữ" },
};

export const AppointmentStatusEnum = {
  NotApproved: 1000,
  Approved: 2000,
  CheckedIn: 3000,
  InProgress: 4000,
  Finished: 9000,
  Canceled: 9999,
  Invalid: 10000,
};

export const AppointmentStatusDisplayConfig = {
  [AppointmentStatusEnum.NotApproved]: {
    title: "Chưa xác nhận",
    color: "default",
  },
  [AppointmentStatusEnum.Approved]: { title: "Đã xác nhận", color: "primary" },
  [AppointmentStatusEnum.CheckedIn]: { title: "Có mặt", color: "default" },
  [AppointmentStatusEnum.InProgress]: {
    title: "Đang khám",
    color: "secondary",
  },
  [AppointmentStatusEnum.Finished]: { title: "Hoàn thành", color: "success" },
  [AppointmentStatusEnum.Canceled]: { title: "Đã huỷ", color: "warning" },
  [AppointmentStatusEnum.Invalid]: { title: "Không hợp lệ", color: "error" },
};
