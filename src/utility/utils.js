export const isObjEmpty = (obj) => Object.keys(obj).length === 0;

export const enumToSelectOptions = (obj) => {
  const keys = Object.keys(obj);
  return keys.map((key) => ({
    value: obj[key],
    label: key,
  }));
};
