import {
  Box,
  Button,
  Container,
  Link,
  TextField,
  Typography,
} from "@mui/material";
import { Formik } from "formik";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { useToast } from "../../context/toastContext";

const Login = () => {
  const navigate = useNavigate();
  //  const dispatch = useDispatch();
  const showToast = useToast();

  //   const handleLogin = async (data) => {
  //     try {
  //       const result = await loginAPI(data);
  //       dispatch(loginAC(result.data));
  //       navigate("/home", { replace: true });
  //     } catch (error) {
  //       showToast({
  //         type: "error",
  //         message: getErrorMessage(error),
  //       });
  //     }
  //   };

  return (
    <>
      <Box
        sx={{
          backgroundColor: "background.default",
          display: "flex",
          flexDirection: "column",
          height: "100%",
          justifyContent: "center",
          mt: 30,
        }}
      >
        <Container maxWidth="sm">
          <Formik
            initialValues={{
              userName: "",
              password: "",
            }}
            validationSchema={Yup.object().shape({
              userName: Yup.string()
                .email("Must be a valid email")
                .max(255)
                .required("Email is required"),
              password: Yup.string().max(255).required("Password is required"),
            })}
            // onSubmit={handleLogin}
          >
            {({
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              isSubmitting,
              touched,
              values,
            }) => (
              <form onSubmit={handleSubmit}>
                <Box sx={{ mb: 3 }}>
                  <Typography color="textPrimary" variant="h2">
                    Đăng Nhập
                  </Typography>
                </Box>
                <TextField
                  error={Boolean(touched.userName && errors.userName)}
                  fullWidth
                  helperText={touched.userName && errors.userName}
                  label="Email"
                  margin="normal"
                  name="userName"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="userName"
                  value={values.userName}
                  variant="outlined"
                />
                <TextField
                  error={Boolean(touched.password && errors.password)}
                  fullWidth
                  helperText={touched.password && errors.password}
                  label="Mật khẩu"
                  margin="normal"
                  name="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="password"
                  value={values.password}
                  variant="outlined"
                />
                <Box sx={{ py: 2 }}>
                  <Button
                    color="primary"
                    disabled={isSubmitting}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                  >
                    Đăng nhập
                  </Button>
                </Box>
                <Box sx={{ display: "flex" }}>
                  <Typography color="textSecondary" variant="body1">
                    <Link
                      component={RouterLink}
                      to="/forgot-password"
                      variant="h6"
                      underline="hover"
                    >
                      Quên mật khẩu
                    </Link>
                  </Typography>
                  <Typography color="textSecondary" variant="body1" ml="auto">
                    <Link
                      component={RouterLink}
                      to="/register"
                      variant="h6"
                      underline="hover"
                    >
                      Đăng Ký
                    </Link>
                  </Typography>
                </Box>
              </form>
            )}
          </Formik>
        </Container>
      </Box>
    </>
  );
};

export default Login;
