import { yupResolver } from "@hookform/resolvers/yup";
import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  MenuItem,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { FormGroup } from "reactstrap";
import * as yup from "yup";
import { getUserInfoAPI, updateUserInfoAPI } from "../../api/main";
import { CityEnum } from "../../utility/constants";
import { enumToSelectOptions } from "../../utility/utils";

const UserSettings = ({ open, handleClose }) => {
  const [email, setEmail] = useState("");

  const UserSettingsSchema = yup.object().shape({
    firstName: yup.string().required().max(128).trim(),
    lastName: yup.string().required().max(128).trim(),
    phoneNumber: yup.string().max(128).trim(),
    cityId: yup.number().required(),
    address: yup.string().max(128).trim(),
  });

  const defaultValues = {
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    cityId: CityEnum.HaNoi,
    address: "",
  };

  const { register, formState, handleSubmit, errors, reset, control } = useForm(
    {
      mode: "onChange",
      resolver: yupResolver(UserSettingsSchema),
      defaultValues,
    }
  );

  useEffect(() => {
    const asyncFn = async () => {
      try {
        const response = await getUserInfoAPI();
        setEmail(response.data.email);
        reset({
          ...defaultValues,
          ...{
            firstName: response.data.firstName,
            lastName: response.data.lastName,
            email: response.data.email,
            phoneNumber: response.data.phoneNumber,
            gender: response.data.gender,
            cityId: response.data.cityId,
            address: response.data.address,
          },
        });
      } catch (error) {
        return console.log(">>error", error);
      }
    };
    asyncFn();
  }, [open]);

  const onSubmit = async (data) => {
    try {
      await updateUserInfoAPI({
        firstName: data.firstName,
        lastName: data.lastName,
        email: email,
        phoneNumber: data.phoneNumber,
        birthdayUnixTime: 0,
        gender: 0,
        cityId: data.cityId,
        address: data.address,
      });
      handleClose("Save");
    } catch (error) {
      console.log(">>error", error);
    }
  };

  const handleReset = () => {
    reset(defaultValues);
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      maxWidth="sm"
      scroll="paper"
    >
      <DialogTitle sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">Tài khoản của tôi</Typography>
        <IconButton
          sx={{ ml: "auto" }}
          aria-label="close"
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <FormGroup>
          <Box sx={{ pl: 3, pr: 3, pt: 1.5 }}>
            <Box sx={{ display: "flex" }}>
              <TextField
                label="Tên"
                variant="outlined"
                id="firstName"
                name="firstName"
                sx={{ width: 250 }}
                inputRef={register()}
              />
              <TextField
                label="Họ"
                variant="outlined"
                id="lastName"
                name="lastName"
                sx={{ ml: 2, width: 250 }}
                inputRef={register()}
              />
            </Box>
            <Box sx={{ display: "flex", mt: 2 }}>
              <TextField
                sx={{ width: 250 }}
                variant="outlined"
                type="email"
                id="email"
                name="email"
                disabled
                value={email}
              />
              <TextField
                label="Số điện thoại"
                variant="outlined"
                id="phoneNumber"
                name="phoneNumber"
                sx={{ ml: 2, width: 250 }}
                inputRef={register()}
              />
            </Box>
            <Box sx={{ display: "flex", mt: 2 }}>
              <Controller
                name="cityId"
                control={control}
                render={({ value, onChange }) => (
                  <TextField
                    id="cityId"
                    select
                    value={value}
                    onChange={onChange}
                    variant="outlined"
                    sx={{ width: 250 }}
                  >
                    {enumToSelectOptions(CityEnum).map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                )}
              />
              <TextField
                label="Địa chỉ"
                variant="outlined"
                id="address"
                name="address"
                fullWidth
                sx={{ ml: 2, width: 250 }}
                inputRef={register()}
              />
            </Box>
          </Box>
        </FormGroup>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={handleReset}>
          Reset
        </Button>
        <Button
          variant="contained"
          disabled={!formState.isDirty}
          onClick={handleSubmit(onSubmit)}
          type="submit"
        >
          Lưu
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default UserSettings;
