import { yupResolver } from "@hookform/resolvers/yup";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  IconButton,
  MenuItem,
  Paper,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import * as yup from "yup";
import { deletePatientAPI, searchPatientAPI } from "../../api/main";
import { GenderEnum, GenderDisplayConfig } from "../../utility/constants";
import { enumToSelectOptions } from "../../utility/utils";
import EditPatientModal from "./EditPatientModal";

const MoreOptions = ({ patient, handleEditModelOpen, handleDeletePatient }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSetEnabled = (data) => {};

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      <IconButton
        aria-describedby={id}
        variant="contained"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <MenuItem>
          <Button
            onClick={() => {
              handleEditModelOpen(patient);
            }}
          >
            Sửa
          </Button>
        </MenuItem>
        <MenuItem>
          <Button
            onClick={() => {
              handleDeletePatient(patient.patientId);
            }}
          >
            Xoá
          </Button>
        </MenuItem>
      </Popover>
    </div>
  );
};

const Patients = () => {
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [filter, setFilter] = useState({
    keyword: "",
    patientCode: "",
    gender: null,
    yearOfBirth: 0,
  });
  const [totalRows, setTotalRows] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [pageNum, setPageNum] = useState(0);
  const [refreshToggle, setRefreshToggle] = useState(false);
  const [patientEditModalOpen, setPatientEditModalOpen] = useState(false);
  const [patient, setPatient] = useState();

  const filterSchema = yup.object().shape({
    keyword: yup.string().max(128),
    patientCode: yup.string(),
    gender: yup.number().nullable(true),
    yearOfBirth: yup.number().nullable(true),
  });

  const defaultValues = {
    keyword: "",
    patientCode: "",
    gender: null,
    yearOfBirth: 0,
  };
  const { register, formState, handleSubmit, errors, reset, control } = useForm(
    {
      mode: "onChange",
      resolver: yupResolver(filterSchema),
      defaultValues,
    }
  );

  const fetchData = async (pageSize, pageNumber, details) => {
    const res = await searchPatientAPI(pageSize, pageNumber, details);
    setData(res.data.pageData);
  };

  useEffect(() => {
    fetchData(rowsPerPage, 1, filter);
  }, [filter, refreshToggle]);

  const handlePageChange = (page) => {
    //fetchData(rowsPerPage, page, keyword);
  };

  const handleRowPerPageChange = async (newPerPage, page) => {
    // fetchData(newPerPage, page, keyword);
    setRowsPerPage(newPerPage);
  };

  const handleSearch = async (data) => {
    setFilter(data);
  };

  const openAddModal = () => {
    setPatientEditModalOpen(true);
  };

  const handleCloseModal = (result) => {
    setPatient();
    setPatientEditModalOpen(false);
    if (result === "Save") {
      setRefreshToggle(!refreshToggle);
    }
  };

  const handleResetFilter = () => {
    reset(defaultValues);
    handleSearch(defaultValues);
  };

  const handleEditModelOpen = (data) => {
    setPatient(data);
    setPatientEditModalOpen(true);
  };

  const handleDeletePatient = async (patientId) => {
    const response = await deletePatientAPI(patientId);
    console.log(response);
    setRefreshToggle(!refreshToggle);
  };

  return (
    <Grid sx={{ display: "flex" }} container>
      <Grid item md={2}>
        <Card sx={{ pt: 3, pl: 4 }}>
          <CardContent>
            <Button
              variant="contained"
              onClick={() => openAddModal()}
              sx={{ width: "80%" }}
            >
              Thêm bệnh nhân
            </Button>
            <Box>
              <TextField
                variant="outlined"
                size="small"
                sx={{ mt: 3 }}
                label="Tên/SĐT"
                name="keyword"
                inputRef={register()}
              />
              <TextField
                variant="outlined"
                size="small"
                sx={{ mt: 3 }}
                label="Mã bệnh nhân"
                name="patientCode"
                inputRef={register()}
              />
              <Controller
                name="gender"
                control={control}
                render={({ value, onChange }) => (
                  <TextField
                    variant="outlined"
                    size="small"
                    sx={{ mt: 3, width: "80%" }}
                    id="gender"
                    select
                    value={value}
                    onChange={onChange}
                    label="Giới tính"
                  >
                    {enumToSelectOptions(GenderEnum).map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {GenderDisplayConfig[option.value].title}
                      </MenuItem>
                    ))}
                  </TextField>
                )}
              />
              <TextField
                type="number"
                variant="outlined"
                size="small"
                sx={{ mt: 3 }}
                label="Năm sinh"
                name="yearOfBirth"
                inputRef={register()}
              />
              <Box
                sx={{
                  display: "flex",
                  mt: 3,
                }}
              >
                <Button
                  variant="contained"
                  onClick={handleSubmit(handleSearch)}
                  sx={{ mr: 5 }}
                >
                  Tìm
                </Button>
                <Button
                  variant="outlined"
                  onClick={() => {
                    handleResetFilter();
                  }}
                >
                  Đặt lại
                </Button>
              </Box>
            </Box>
          </CardContent>
        </Card>
      </Grid>
      <Grid item md={10}>
        <TableContainer sx={{ pt: 4, ml: 2, pl: 2 }} component={Paper}>
          <Table sx={{ minWidth: 650 }}>
            <TableHead>
              <TableRow sx={{ backgroundColor: "#F3F2F7" }}>
                <TableCell sx={{ fontWeight: "700" }} align="left">
                  TÊN BỆNH NHÂN
                </TableCell>
                <TableCell sx={{ fontWeight: "700" }} align="left">
                  MÃ BỆNH NHÂN
                </TableCell>
                <TableCell sx={{ fontWeight: "700" }} align="left">
                  SỐ ĐIỆN THOẠI
                </TableCell>
                <TableCell sx={{ fontWeight: "700" }} align="left">
                  ĐỊA CHỈ
                </TableCell>
                <TableCell sx={{ fontWeight: "700" }} align="left"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow
                  key={row.patientId}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell align="left">{row.fullName}</TableCell>
                  <TableCell align="left">{row.patientCode}</TableCell>
                  <TableCell align="left">{row.phoneNumber}</TableCell>
                  <TableCell align="left">{row.address}</TableCell>
                  <TableCell align="left">
                    <MoreOptions
                      patient={row}
                      handleEditModelOpen={handleEditModelOpen}
                      handleDeletePatient={handleDeletePatient}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
            <TablePagination
              rowsPerPageOptions={[5, 10, 15, 20]}
              count={totalRows}
              page={pageNum}
              rowsPerPage={rowsPerPage}
              onPageChange={handlePageChange}
              onRowsPerPageChange={handleRowPerPageChange}
            />
          </Table>
          {patientEditModalOpen && (
            <EditPatientModal
              patient={patient}
              open={patientEditModalOpen}
              handleClose={handleCloseModal}
            />
          )}
        </TableContainer>
      </Grid>
    </Grid>
  );
};

export default Patients;
