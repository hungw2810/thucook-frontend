import { yupResolver } from "@hookform/resolvers/yup";
import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  IconButton,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@mui/material";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { useEffect } from "react";
import { Controller, useForm } from "react-hook-form";
import { FormGroup, Label } from "reactstrap";
import * as yup from "yup";
import {
  createPatientAPI,
  getDetailsPatientAPI,
  updatePatientAPI,
} from "../../api/main";

const EditPatientModal = ({ open, handleClose, patient }) => {
  const EditPatientModalSchema = yup.object().shape({
    fullName: yup.string().required().max(256).trim(),
    birthdayUnixTime: yup.date().required(),
    gender: yup.number().required(),
    phoneNumber: yup.string().required().max(128).trim(),
    address: yup.string().required().max(256).trim(),
    email: yup.string().required().max(128).trim(),
    heightInCm: yup.number().min(0).max(32767),
    weightInKg: yup.number().min(0).max(32767),
    allergy: yup.string().max(1024).trim(),
  });

  const defaultValues = {
    fullName: "",
    birthdayUnixTime: null,
    gender: 1,
    phoneNumber: "",
    address: "",
    email: "",
    heightInCm: 0,
    weightInKg: 0,
    allergy: "",
  };

  const { register, formState, handleSubmit, errors, reset, control } = useForm(
    {
      mode: "onChange",
      resolver: yupResolver(EditPatientModalSchema),
      defaultValues,
    }
  );

  const onSubmit = async (data) => {
    try {
      if (!!patient) {
        await updatePatientAPI(patient.patientId, {
          fullName: data.fullName,
          birthdayUnixTime: data.birthdayUnixTime.getTime(),
          gender: data.gender,
          phoneNumber: data.phoneNumber,
          address: data.address,
          email: data.email,
          heightInCm: data.heightInCm,
          weightInKg: data.weightInKg,
          allergy: data.allergy,
        });
      } else {
        await createPatientAPI({
          fullName: data.fullName,
          birthdayUnixTime: data.birthdayUnixTime.getTime(),
          gender: data.gender,
          phoneNumber: data.phoneNumber,
          address: data.address,
          email: data.email,
          heightInCm: data.heightInCm,
          weightInKg: data.weightInKg,
          allergy: data.allergy,
        });
      }
      handleClose("Save");
    } catch (error) {
      console.log(">>error", error);
    }
  };

  useEffect(() => {
    const asyncFn = async () => {
      if (patient) {
        try {
          const response = await getDetailsPatientAPI(patient.patientId);
          reset({
            ...defaultValues,
            ...{
              fullName: response.data.fullName,
              birthdayUnixTime: new Date(response.data.birthdayUnix),
              gender: response.data.gender,
              phoneNumber: response.data.phoneNumber,
              address: response.data.address,
              email: response.data.email,
              heightInCm: response.data.heightInCm,
              weightInKg: response.data.weightInKg,
              allergy: response.data.allergy,
            },
          });
        } catch (error) {
          return console.log(">>error", error);
        }
      }
    };
    asyncFn();
  }, [patient, reset]);

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      maxWidth="md"
      scroll="body"
    >
      <DialogTitle sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">
          {!!patient ? "Chỉnh sửa hồ sơ" : "Tạo hồ sơ"}
        </Typography>
        <IconButton
          sx={{ ml: "auto" }}
          aria-label="close"
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent sx={{ p: 5 }}>
        <FormGroup>
          <Box display="flex">
            <Box sx={{ mr: 4 }}>
              <Label for="fullName">
                <Box sx={{ display: "flex" }}>
                  <Typography>Tên bệnh nhân</Typography>
                  <span className="text-danger">&nbsp;*</span>
                </Box>
              </Label>
              <TextField
                id="fullName"
                name="fullName"
                variant="outlined"
                inputRef={register()}
                sx={{ width: 350 }}
              />
            </Box>
            <Box>
              <Label for="birthdayUnixTime">
                <Box sx={{ display: "flex" }}>
                  <Typography>Ngày tháng năm sinh</Typography>
                  <span className="text-danger">&nbsp;*</span>
                </Box>
              </Label>
              <Controller
                name="birthdayUnixTime"
                control={control}
                render={({ value, onChange }) => (
                  <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DatePicker
                      disableFuture
                      inputFormat="dd/MM/yyyy"
                      value={value}
                      onChange={onChange}
                      renderInput={(params) => <TextField {...params} />}
                    />
                  </LocalizationProvider>
                )}
              />
            </Box>
          </Box>
          <Box sx={{ display: "flex", mt: 3 }}>
            <Box fullWidth sx={{ mr: 4 }}>
              <Label for="gender">
                <Box sx={{ display: "flex" }}>
                  <Typography>Giới tính</Typography>
                  <span className="text-danger">&nbsp;*</span>
                </Box>
              </Label>
              <Controller
                name="gender"
                control={control}
                render={({ value, onChange }) => (
                  <RadioGroup name="gender" value={value} onChange={onChange}>
                    <Box sx={{ display: "flex" }}>
                      <FormControlLabel
                        value={1}
                        control={<Radio />}
                        label="Nữ"
                      />
                      <FormControlLabel
                        value={0}
                        control={<Radio />}
                        label="Nam"
                        sx={{ ml: 5 }}
                      />
                    </Box>
                  </RadioGroup>
                )}
              />
            </Box>
            <Box sx={{ ml: 18.5 }}>
              <Label for="phoneNumber">
                <Box sx={{ display: "flex" }}>
                  <Typography>Số điện thoại</Typography>
                  <span className="text-danger">&nbsp;*</span>
                </Box>
              </Label>
              <TextField
                id="phoneNumber"
                name="phoneNumber"
                variant="outlined"
                inputRef={register()}
                sx={{ width: 350 }}
              />
            </Box>
          </Box>
          <Box sx={{ display: "flex", mt: 3 }}>
            <Box sx={{ mr: 4 }}>
              <Label for="address">
                <Box sx={{ display: "flex" }}>
                  <Typography>Địa chỉ</Typography>
                  <span className="text-danger">&nbsp;*</span>
                </Box>
              </Label>
              <TextField
                id="address"
                name="address"
                variant="outlined"
                inputRef={register()}
                sx={{ width: 350 }}
              />
            </Box>
            <Box>
              <Label for="email">
                <Box sx={{ display: "flex" }}>
                  <Typography>Email</Typography>
                  <span className="text-danger">&nbsp;*</span>
                </Box>
              </Label>
              <TextField
                id="email"
                name="email"
                variant="outlined"
                inputRef={register()}
                sx={{ width: 350 }}
              />
            </Box>
          </Box>
          <Box sx={{ display: "flex", mt: 3 }}>
            <Box sx={{ mr: 4 }}>
              <Label for="heightInCm">
                <Box sx={{ display: "flex" }}>
                  <Typography>Chiều cao</Typography>
                </Box>
              </Label>
              <TextField
                id="heightInCm"
                name="heightInCm"
                type="number"
                variant="outlined"
                inputRef={register()}
                sx={{ width: 350 }}
              />
            </Box>
            <Box>
              <Label for="weightInKg">
                <Box sx={{ display: "flex" }}>
                  <Typography>Cân nặng</Typography>
                </Box>
              </Label>
              <TextField
                id="weightInKg"
                name="weightInKg"
                type="number"
                variant="outlined"
                inputRef={register()}
                sx={{ width: 350 }}
              />
            </Box>
          </Box>
          <Box sx={{ mt: 3 }}>
            <Label for="allergy">
              <Box sx={{ display: "flex" }}>
                <Typography>Tiền sử dị ứng</Typography>
              </Box>
            </Label>
            <TextField
              id="allergy"
              name="allergy"
              variant="outlined"
              inputRef={register()}
              sx={{ width: 350 }}
            />
          </Box>
        </FormGroup>
      </DialogContent>
      <DialogActions>
        <Button
          variant="outlined"
          onClick={() => {
            handleClose();
          }}
        >
          Huỷ
        </Button>
        <Button
          variant="contained"
          disabled={!formState.isDirty}
          onClick={handleSubmit(onSubmit)}
          type="submit"
        >
          Lưu
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EditPatientModal;
