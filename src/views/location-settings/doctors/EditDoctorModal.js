import { yupResolver } from "@hookform/resolvers/yup";
import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Tab,
  Tabs,
  Typography,
} from "@mui/material";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import {
  getAllSpecialtiesAPI,
  getDetailsDoctorAPI,
  updateDoctorAPI,
} from "../../../api/main";
import DoctorSettingsTab from "./DoctorSettingsTab";
import GeneralTabContent from "./GeneralTabContent";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Box>{children}</Box>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const EditDoctorModal = ({ open, handleClose, doctor }) => {
  const [value, setValue] = useState(0);
  const [specialties, setSpecialties] = useState();

  const EditDoctorSchema = yup.object().shape({
    doctorName: yup.string().max(128).trim().required(),
    doctorCode: yup.string().max(128).trim(),
    phoneNumber: yup.string().max(128).trim().required(),
    email: yup.string().max(128).trim().required(),
    yearOfExperience: yup.number().nullable(true),
    specialtyId: yup.string().max(36).nullable(true),
    timePerAppointmentInMinutes: yup.number().min(0).required(),
    bufferTimePerAppointmentInMinutes: yup.number().min(0).required(),
    isVisibleForBooking: yup.bool().required(),
  });

  const defaultValues = {
    doctorName: "",
    doctorCode: "",
    phoneNumber: "",
    email: "",
    yearOfExperience: 0,
    specialtyId: "",
    timePerAppointmentInMinutes: 0,
    bufferTimePerAppointmentInMinutes: 0,
    isVisibleForBooking: true,
  };

  const { register, formState, handleSubmit, errors, control, reset } = useForm(
    {
      mode: "onChange",
      resolver: yupResolver(EditDoctorSchema),
      defaultValues,
    }
  );

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    const asyncFn = async () => {
      const response = await getAllSpecialtiesAPI();
      setSpecialties(response);
      if (doctor) {
        try {
          const response = await getDetailsDoctorAPI(doctor.doctorId);
          reset({
            ...defaultValues,
            ...{
              doctorName: response.data.doctorName,
              doctorCode: response.data.doctorCode,
              phoneNumber: response.data.phoneNumber,
              email: response.data.email,
              yearOfExperience: response.data.yearOfExperience,
              specialtyId: response.data.specialtyId,
              timePerAppointmentInMinutes:
                response.data.timePerAppointmentInMinutes,
              bufferTimePerAppointmentInMinutes:
                response.data.bufferTimePerAppointmentInMinutes,
              isVisibleForBooking: response.data.isVisibleForBooking,
            },
          });
        } catch (error) {
          return console.log(">>error", error);
        }
      }
    };
    asyncFn();
  }, [doctor, reset]);

  const onSubmit = async (data) => {
    console.log(data);
    try {
      if (!!doctor) {
        await updateDoctorAPI(doctor.doctorId, {
          doctorCode: data.doctorCode,
          phoneNumber: data.phoneNumber,
          email: data.email,
          yearOfExperience: data.yearOfExperience,
          specialtyId: data.specialtyId,
          timePerAppointmentInMinutes: data.timePerAppointmentInMinutes,
          bufferTimePerAppointmentInMinutes:
            data.bufferTimePerAppointmentInMinutes,
          isVisibleForBooking: data.isVisibleForBooking,
        });
      } else {
      }
      handleClose("Save");
    } catch (error) {
      console.log(">>error", error);
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullwidth
      maxWidth="md"
      scroll="body"
    >
      <DialogTitle sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">{doctor ? "Sửa" : "Thêm bác sĩ"}</Typography>
        <IconButton
          sx={{ ml: "auto" }}
          aria-label="close"
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent sx={{ pl: 4 }}>
        <Box
          sx={{
            flexGrow: 1,
            bgcolor: "background.paper",
            display: "flex",
          }}
        >
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={value}
            onChange={handleChange}
            aria-label="Vertical tabs example"
            sx={{ borderRight: 1, borderColor: "divider" }}
          >
            <Tab label="Thông tin chung" {...a11yProps(0)} />
            <Tab label="Cài đặt" {...a11yProps(1)} />
          </Tabs>
          <Box sx={{ ml: 4 }}>
            <TabPanel value={value} index={0}>
              <GeneralTabContent
                specialties={specialties}
                register={register}
                control={control}
              />
            </TabPanel>
            <TabPanel value={value} index={1}>
              <DoctorSettingsTab register={register} control={control} />
            </TabPanel>
          </Box>
        </Box>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={() => handleClose()}>
          Huỷ
        </Button>
        <Button
          variant="contained"
          disabled={!formState.isDirty}
          onClick={handleSubmit(onSubmit)}
          type="submit"
        >
          Lưu
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EditDoctorModal;
