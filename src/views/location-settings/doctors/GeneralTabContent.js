import { Box, MenuItem, TextField, Typography } from "@mui/material";
import { Controller } from "react-hook-form";
import { Label } from "reactstrap";

const GeneralTabContent = ({ specialties, register, control }) => {
  return (
    <>
      <Box sx={{ display: "flex" }}>
        <Box>
          <Label for="doctorName">
            <Box sx={{ display: "flex" }}>
              <Typography>Tên bác sĩ</Typography>
              <span className="text-danger">&nbsp;*</span>
            </Box>
          </Label>
          <TextField
            id="doctorName"
            name="doctorName"
            variant="outlined"
            inputRef={register()}
            sx={{ width: 250 }}
          />
        </Box>
        <Box sx={{ ml: 2 }}>
          <Label for="doctorCode">
            <Typography>Mã bác sĩ</Typography>
          </Label>
          <TextField
            id="doctorCode"
            name="doctorCode"
            variant="outlined"
            inputRef={register()}
            sx={{ width: 250 }}
          />
        </Box>
      </Box>
      <Box sx={{ mt: 2 }}>
        <Label for="specialtyId">
          <Box sx={{ display: "flex" }}>
            <Typography>Chuyên khoa</Typography>
            <span className="text-danger">&nbsp;*</span>
          </Box>
        </Label>
        <Controller
          name="specialtyId"
          control={control}
          render={({ value, onChange }) => (
            <TextField
              fullWidth
              id="specialtyId"
              select
              value={value}
              onChange={onChange}
              variant="outlined"
            >
              {specialties?.map((specialty) => (
                <MenuItem
                  key={specialty.specialtyId}
                  value={specialty.specialtyId}
                >
                  {specialty.specialtyName}
                </MenuItem>
              ))}
            </TextField>
          )}
        />
      </Box>
      <Box sx={{ display: "flex", mt: 2 }}>
        <Box>
          <Label for="phoneNumber">
            <Box sx={{ display: "flex" }}>
              <Typography>Số điện thoại</Typography>
              <span className="text-danger">&nbsp;*</span>
            </Box>
          </Label>
          <TextField
            type="number"
            id="phoneNumber"
            name="phoneNumber"
            variant="outlined"
            inputRef={register()}
            sx={{ width: 250 }}
          />
        </Box>
        <Box sx={{ ml: 2 }}>
          <Label for="email">
            <Box sx={{ display: "flex" }}>
              <Typography>Email</Typography>
              <span className="text-danger">&nbsp;*</span>
            </Box>
          </Label>
          <TextField
            type="email"
            id="email"
            name="email"
            variant="outlined"
            inputRef={register()}
            sx={{ width: 250 }}
          />
        </Box>
      </Box>
    </>
  );
};

export default GeneralTabContent;
