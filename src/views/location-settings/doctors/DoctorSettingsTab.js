import { Box, Switch, TextField, Typography } from "@mui/material";
import { Controller } from "react-hook-form";
import { Label } from "reactstrap";

const DoctorSettingsTab = ({ register, control }) => {
  return (
    <>
      <Box sx={{ display: "flex" }}>
        <Box>
          <Label for="timePerAppointmentInMinutes">
            <Box sx={{ display: "flex" }}>
              <Typography>Thời gian mỗi ca khám (phút)</Typography>
              <span className="text-danger">&nbsp;*</span>
            </Box>
          </Label>
          <TextField
            id="timePerAppointmentInMinutes"
            name="timePerAppointmentInMinutes"
            variant="outlined"
            inputRef={register()}
            sx={{ width: 250 }}
          />
        </Box>
        <Box sx={{ ml: 2 }}>
          <Label for="bufferTimePerAppointmentInMinutes">
            <Box sx={{ display: "flex" }}>
              <Typography>Thời gian dự phòng (phút)</Typography>
              <span className="text-danger">&nbsp;*</span>
            </Box>
          </Label>
          <TextField
            id="bufferTimePerAppointmentInMinutes"
            name="bufferTimePerAppointmentInMinutes"
            variant="outlined"
            inputRef={register()}
            sx={{ width: 250 }}
          />
        </Box>
      </Box>
      <Box sx={{ mt: 2 }}>
        <Label for="isVisibleForBooking">
          <Typography>Hiển thị trên ứng dụng</Typography>
        </Label>
        <Controller
          name="isVisibleForBooking"
          control={control}
          defaultValue={false}
          render={(props) => (
            <Switch
              fullWidth
              id="isVisibleForBooking"
              onChange={(e) => props.onChange(e.target.checked)}
              checked={props.value}
              variant="outlined"
            />
          )}
        />
      </Box>
    </>
  );
};

export default DoctorSettingsTab;
