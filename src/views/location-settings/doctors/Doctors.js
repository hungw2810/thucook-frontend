import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
  Box,
  Button,
  Card,
  IconButton,
  MenuItem,
  Paper,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
} from "@mui/material";
import { useEffect, useState } from "react";
import {
  deleteDoctorAPI,
  searchDoctorsAPI,
  setEnabledDoctorAPI,
} from "../../../api/main";
import EditDoctorModal from "./EditDoctorModal";

const MoreOptions = ({
  doctor,
  handleEditModelOpen,
  handleDisabledDoctor,
  handleDeleteDoctor,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      <IconButton
        aria-describedby={id}
        variant="contained"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <MenuItem>
          <Button
            onClick={() => {
              handleEditModelOpen(doctor);
            }}
          >
            Sửa
          </Button>
        </MenuItem>
        <MenuItem>
          {doctor.isEnabled ? (
            <Button
              onClick={() => {
                handleDisabledDoctor(doctor.doctorId, false);
              }}
            >
              Dừng hoạt động
            </Button>
          ) : (
            <Button
              onClick={() => {
                handleDisabledDoctor(doctor.doctorId, true);
              }}
            >
              Kích hoạt
            </Button>
          )}
        </MenuItem>
        <MenuItem>
          <Button onClick={() => handleDeleteDoctor(doctor.doctorId)}>
            Xoá
          </Button>
        </MenuItem>
      </Popover>
    </div>
  );
};

const Doctors = () => {
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [totalRows, setTotalRows] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [pageNum, setPageNum] = useState(0);
  const [refreshToggle, setRefreshToggle] = useState(false);
  const [doctorEditModalOpen, setDoctorEditModalOpen] = useState(false);
  const [doctor, setDoctor] = useState();

  const fetchData = async (pageSize, pageNumber, keyword) => {
    const res = await searchDoctorsAPI(pageSize, pageNumber, keyword);
    setData(res.data.pageData);
  };

  useEffect(() => {
    fetchData(rowsPerPage, 1, keyword);
  }, [refreshToggle, keyword]);

  const handleSearch = (e) => {
    setKeyword(e.target.value);
  };

  const handlePageChange = (page) => {
    fetchData(rowsPerPage, page, keyword);
  };

  const handleRowPerPageChange = async (newPerPage, page) => {
    fetchData(newPerPage, page, keyword);
    setRowsPerPage(newPerPage);
  };

  const openAddModal = () => {
    setDoctorEditModalOpen(true);
  };

  const handleCloseModal = (result) => {
    setDoctorEditModalOpen(false);
    setDoctor();
    if (result === "Save") {
      setRefreshToggle(!refreshToggle);
    }
  };

  const handleEditModelOpen = (data) => {
    setDoctor(data);
    setDoctorEditModalOpen(true);
  };

  const handleDisabledDoctor = async (specialityId, isEnabled) => {
    const response = await setEnabledDoctorAPI(specialityId, {
      isEnabled: isEnabled,
    });
    console.log(response);
    setRefreshToggle(!refreshToggle);
  };

  const handleDeleteDoctor = async (specialtyId) => {
    const response = await deleteDoctorAPI(specialtyId);
    console.log(response);
    setRefreshToggle(!refreshToggle);
  };
  return (
    <>
      <Card
        sx={{
          width: "600px",
          mb: 3,
        }}
      >
        <Box
          sx={{
            p: 2,
            display: "flex",
          }}
        >
          <TextField
            variant="outlined"
            label="Tìm kiếm"
            onChange={handleSearch}
            sx={{ width: 420 }}
          />
          <Button
            variant="contained"
            onClick={() => openAddModal()}
            sx={{ ml: 3 }}
          >
            Thêm mới
          </Button>
        </Box>
      </Card>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }}>
          <TableHead>
            <TableRow sx={{ backgroundColor: "#F3F2F7" }}>
              <TableCell sx={{ fontWeight: "700" }} align="center">
                TÊN BÁC SĨ
              </TableCell>
              <TableCell sx={{ fontWeight: "700" }} align="center">
                MÃ BÁC SĨ
              </TableCell>
              <TableCell sx={{ fontWeight: "700" }} align="center">
                CHUYÊN KHOA
              </TableCell>
              <TableCell sx={{ fontWeight: "700" }} align="right"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((row) => (
              <TableRow
                key={row.doctorId}
                sx={{
                  backgroundColor: row.isEnabled ? "white" : "#EBECF0",
                  opacity: row.isEnabled ? 1 : 0.5,
                }}
              >
                <TableCell align="center">{row.doctorName}</TableCell>
                <TableCell align="center">{row.doctorCode}</TableCell>
                <TableCell align="center">{row.specialtyName}</TableCell>
                <TableCell align="center">
                  <MoreOptions
                    doctor={row}
                    handleEditModelOpen={handleEditModelOpen}
                    handleDisabledDoctor={handleDisabledDoctor}
                    handleDeleteDoctor={handleDeleteDoctor}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TablePagination
            rowsPerPageOptions={[5, 10, 15, 20]}
            count={totalRows}
            page={pageNum}
            rowsPerPage={rowsPerPage}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleRowPerPageChange}
          />
        </Table>
        {doctorEditModalOpen && (
          <EditDoctorModal
            doctor={doctor}
            open={doctorEditModalOpen}
            handleClose={handleCloseModal}
          />
        )}
      </TableContainer>
    </>
  );
};

export default Doctors;
