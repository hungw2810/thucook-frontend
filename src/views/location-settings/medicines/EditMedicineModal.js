import { React, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import {
  Dialog,
  DialogTitle,
  Typography,
  IconButton,
  DialogContent,
  Box,
  DialogActions,
  Button,
  TextField,
  MenuItem,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { FormGroup, Label } from "reactstrap";
import { addMedicineAPI, updateMedicineAPI } from "../../../api/main";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { MedicineUnitTypeEnum } from "../../../utility/constants";
import { enumToSelectOptions } from "../../../utility/utils";
import { getDetailsMedicineAPI } from "../../../api/main";

const EditMedicineModal = ({ open, handleClose, medicine }) => {
  const EditMedicineSchema = yup.object().shape({
    medicineName: yup.string().required().max(128).trim(),
    medicineUnitTypeId: yup.number().required(),
    quantity: yup.number().required().positive(),
  });

  const defaultValues = {
    medicineName: "",
    medicineUnitTypeId: MedicineUnitTypeEnum.Vỉ,
    quantity: 0,
  };

  const { register, formState, handleSubmit, errors, reset, control } = useForm(
    {
      mode: "onChange",
      resolver: yupResolver(EditMedicineSchema),
      defaultValues,
    }
  );

  useEffect(() => {
    const asyncFn = async () => {
      if (medicine) {
        try {
          const response = await getDetailsMedicineAPI(medicine.medicineId);
          reset({
            ...defaultValues,
            ...{
              medicineName: response.data.medicineName,
              medicineUnitTypeId: response.data.medicineUnitTypeId,
              quantity: response.data.quantity,
            },
          });
        } catch (error) {
          return console.log(">>error", error);
        }
      }
    };
    asyncFn();
  }, [medicine, reset]);

  const onSubmit = async (data) => {
    try {
      if (!!medicine) {
        await updateMedicineAPI(medicine.medicineId, {
          medicineName: data.medicineName,
          medicineUnitTypeId: data.medicineUnitTypeId,
          quantity: data.quantity,
        });
      } else {
        await addMedicineAPI({
          medicineName: data.medicineName,
          medicineUnitTypeId: data.medicineUnitTypeId,
          quantity: data.quantity,
        });
      }
      handleClose("Save");
    } catch (error) {
      return console.log(">>error", error);
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      maxWidth="sm"
      scroll="body"
    >
      <DialogTitle sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">{!!medicine ? "Sửa" : "Thêm mới"}</Typography>
        <IconButton
          sx={{ ml: "auto" }}
          aria-label="close"
          onClick={() => {
            handleClose();
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <FormGroup>
          <Box sx={{ pl: 3, pr: 3 }}>
            <Label for="medicineName">
              <Box sx={{ display: "flex" }}>
                <Typography>Tên dược phẩm</Typography>
                <span className="text-danger">&nbsp;*</span>
              </Box>
            </Label>
            <TextField
              id="medicineName"
              name="medicineName"
              variant="outlined"
              inputRef={register()}
              autoFocus
              fullWidth
            />
            <Box sx={{ display: "flex", mt: 2 }}>
              <Box>
                <Label for="medicineUnitTypeId">
                  <Box sx={{ display: "flex" }}>
                    <Typography>Đơn vị</Typography>
                    <span className="text-danger">&nbsp;*</span>
                  </Box>
                </Label>
                <Controller
                  name="medicineUnitTypeId"
                  control={control}
                  render={({ value, onChange }) => (
                    <TextField
                      id="medicineUnitTypeId"
                      select
                      value={value}
                      onChange={onChange}
                      variant="outlined"
                      sx={{ width: 245 }}
                    >
                      {enumToSelectOptions(MedicineUnitTypeEnum).map(
                        (option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        )
                      )}
                    </TextField>
                  )}
                />
              </Box>
              <Box sx={{ ml: 2.5 }}>
                <Label for="quantity">
                  <Box sx={{ display: "flex" }}>
                    <Typography>Số lượng</Typography>
                    <span className="text-danger">&nbsp;*</span>
                  </Box>
                </Label>
                <TextField
                  id="quantity"
                  name="quantity"
                  variant="outlined"
                  inputRef={register()}
                  sx={{ width: 240 }}
                />
              </Box>
            </Box>
          </Box>
        </FormGroup>
      </DialogContent>
      <DialogActions>
        <Button
          variant="outlined"
          onClick={() => {
            handleClose();
          }}
        >
          Huỷ
        </Button>
        <Button
          variant="contained"
          disabled={!formState.isDirty}
          onClick={handleSubmit(onSubmit)}
          type="submit"
        >
          Lưu
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EditMedicineModal;
