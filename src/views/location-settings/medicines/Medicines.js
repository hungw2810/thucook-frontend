import { React, useState, useEffect, useCallback } from "react";
import { searchMedicinesAPI } from "../../../api/main";
import {
  Table,
  TableContainer,
  TableBody,
  TableCell,
  Paper,
  TableHead,
  TableRow,
  Box,
  Button,
  IconButton,
  Popover,
  MenuItem,
  TablePagination,
  TextField,
  Card,
} from "@mui/material";
import EditMedicineModal from "./EditMedicineModal";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { MedicineUnitTypeDisplayConfig } from "../../../utility/constants";
import { deleteMedicineAPI } from "../../../api/main";

const MoreOptions = ({
  medicine,
  handleEditModelOpen,
  handleDeleteMedicine,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      <IconButton
        aria-describedby={id}
        variant="contained"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <MenuItem>
          <Button
            onClick={() => {
              handleEditModelOpen(medicine);
            }}
          >
            Sửa
          </Button>
        </MenuItem>
        <MenuItem>
          <Button
            onClick={() => {
              handleDeleteMedicine(medicine.medicineId);
            }}
          >
            Xoá
          </Button>
        </MenuItem>
      </Popover>
    </div>
  );
};

const Medicines = () => {
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [totalRows, setTotalRows] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [pageNum, setPageNum] = useState(0);
  const [refreshToggle, setRefreshToggle] = useState(false);
  const [medicineEditModalOpen, setMedicineEditModalOpen] = useState(false);
  const [medicine, setMedicine] = useState();

  const fetchData = useCallback(async (pageSize, pageNumber, keyword) => {
    const response = await searchMedicinesAPI(pageSize, pageNumber, keyword);
    setData(response.data.pageData);
    setTotalRows(response.data.paging.totalItem);
  }, []);

  useEffect(() => {
    fetchData(rowsPerPage, 1, keyword);
  }, [refreshToggle, keyword]);

  const handleSearch = (e) => {
    setKeyword(e.target.value);
  };

  const handlePageChange = (page) => {
    fetchData(rowsPerPage, page, keyword);
  };

  const handleRowPerPageChange = async (newPerPage, page) => {
    fetchData(newPerPage, page, keyword);
    setRowsPerPage(newPerPage);
  };

  const openAddModal = () => {
    setMedicineEditModalOpen(true);
  };

  const handleCloseModal = (result) => {
    setMedicineEditModalOpen(false);
    setMedicine(null);
    if (result === "Save") {
      setRefreshToggle(!refreshToggle);
    }
  };

  const handleEditModelOpen = (data) => {
    setMedicine(data);
    setMedicineEditModalOpen(true);
  };

  const handleDeleteMedicine = async (medicineId) => {
    const response = await deleteMedicineAPI(medicineId);
    console.log(response);
    setRefreshToggle(!refreshToggle);
  };

  return (
    <>
      <Card
        sx={{
          width: "600px",
          mb: 3,
        }}
      >
        <Box
          sx={{
            p: 2,
            display: "flex",
          }}
        >
          <TextField
            variant="outlined"
            label="Tìm kiếm"
            onChange={handleSearch}
            sx={{ width: 420 }}
          />
          <Button
            variant="contained"
            onClick={() => openAddModal()}
            sx={{ ml: 3 }}
          >
            Thêm mới
          </Button>
        </Box>
      </Card>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }}>
          <TableHead>
            <TableRow sx={{ backgroundColor: "#F3F2F7" }}>
              <TableCell sx={{ fontWeight: "600" }} align="center">
                TÊN THUỐC
              </TableCell>
              <TableCell sx={{ fontWeight: "600" }} align="center">
                ĐƠN VỊ
              </TableCell>
              <TableCell sx={{ fontWeight: "600" }} align="center">
                SỐ LƯỢNG
              </TableCell>
              <TableCell align="center"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((row) => (
              <TableRow
                key={row.medicineId}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row" align="center">
                  {row.medicineName}
                </TableCell>
                <TableCell align="center">
                  {MedicineUnitTypeDisplayConfig[row.medicineUnitTypeId].title}
                </TableCell>
                <TableCell align="center">{row.quantity}</TableCell>
                <TableCell align="center">
                  {row.isEditable && (
                    <MoreOptions
                      medicine={row}
                      handleEditModelOpen={handleEditModelOpen}
                      handleDeleteMedicine={handleDeleteMedicine}
                    />
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TablePagination
            rowsPerPageOptions={[5, 10, 15, 20]}
            count={totalRows}
            page={pageNum}
            rowsPerPage={rowsPerPage}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleRowPerPageChange}
          />
        </Table>
        {medicineEditModalOpen && (
          <EditMedicineModal
            medicine={medicine}
            open={medicineEditModalOpen}
            handleClose={handleCloseModal}
          />
        )}
      </TableContainer>
    </>
  );
};

export default Medicines;
