import { Box, Grid, Tab, Tabs } from "@mui/material";
import PropTypes from "prop-types";
import { useState } from "react";
import Doctors from "./doctors/Doctors";
import Medicines from "./medicines/Medicines";
import Specialties from "./specialties/Specialties";
import Symptoms from "./symptoms/Symptoms";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Box>{children}</Box>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const LocationSettings = () => {
  const [data, setData] = useState();
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={3} sx={{ mt: 3 }}>
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={value}
            onChange={handleChange}
            sx={{ borderRight: 1, borderColor: "divider" }}
          >
            <Tab label="Chuyên khoa" {...a11yProps(0)} />
            <Tab label="Bác sĩ" {...a11yProps(1)} />
            <Tab label="Nhân viên" {...a11yProps(2)} />
            <Tab label="Triệu chứng gợi ý" {...a11yProps(3)} />
            <Tab label="Dược phẩm" {...a11yProps(4)} />
          </Tabs>
        </Grid>
        <Grid item xs={9}>
          <TabPanel value={value} index={0}>
            <Specialties />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <Doctors />
          </TabPanel>
          <TabPanel value={value} index={2}>
            Item Three
          </TabPanel>
          <TabPanel value={value} index={3}>
            <Symptoms />
          </TabPanel>
          <TabPanel value={value} index={4}>
            <Medicines />
          </TabPanel>
        </Grid>
      </Grid>
    </Box>
  );
};

export default LocationSettings;
