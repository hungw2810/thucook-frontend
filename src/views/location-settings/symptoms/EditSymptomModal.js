import { yupResolver } from "@hookform/resolvers/yup";
import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  MenuItem,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { FormGroup, Label } from "reactstrap";
import * as yup from "yup";
import {
  createSymptomAPI,
  getAllSpecialtiesAPI,
  getDetailsSymptomAPI,
  updateSymptomAPI,
} from "../../../api/main";

const EditSymptomModal = ({ open, handleClose, symptom }) => {
  const [specialties, setSpecialties] = useState();
  const [specialtyId, setSpecialtyId] = useState(null);
  const EditSymptomSchema = yup.object().shape({
    specialtyId: yup.string().nullable().max(36).trim(),
    symptomName: yup.string().required().max(128).trim(),
    content: yup.string().required().max(1024).trim(),
  });

  const defaultValues = {
    specialtyId: null,
    symptomName: "",
    content: "",
  };

  const { register, formState, handleSubmit, errors, reset, control } = useForm(
    {
      mode: "onChange",
      resolver: yupResolver(EditSymptomSchema),
      defaultValues,
    }
  );

  useEffect(() => {
    const asyncFn = async () => {
      const response = await getAllSpecialtiesAPI();
      setSpecialties(response);
      if (symptom) {
        try {
          const response = await getDetailsSymptomAPI(symptom.symptomId);
          reset({
            ...defaultValues,
            ...{
              specialtyId: response.data.specialtyId,
              symptomName: response.data.symptomName,
              content: response.data.content,
            },
          });
        } catch (error) {
          return console.log(">>error", error);
        }
      }
    };
    asyncFn();
  }, [symptom, reset]);

  const onSubmit = async (data) => {
    try {
      if (!!symptom) {
        await updateSymptomAPI(symptom.symptomId, {
          specialtyId: data.specialtyId,
          symptomName: data.symptomName,
          content: data.content,
        });
      } else {
        await createSymptomAPI({
          specialtyId: data.specialtyId,
          symptomName: data.symptomName,
          content: data.content,
        });
      }
      handleClose("Save");
    } catch (error) {
      return console.log(">>error", error);
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      maxWidth="sm"
      scroll="body"
    >
      <DialogTitle sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">{!!symptom ? "Sửa" : "Thêm mới"}</Typography>
        <IconButton
          sx={{ ml: "auto" }}
          aria-label="close"
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <FormGroup>
          <Box sx={{ pl: 3, pr: 3 }}>
            <Box sx={{ display: "flex" }}>
              <Box>
                <Label for="symptomName">
                  <Box sx={{ display: "flex" }}>
                    <Typography>Tên triệu chứng</Typography>
                    <span className="text-danger">&nbsp;*</span>
                  </Box>
                </Label>
                <TextField
                  id="symptomName"
                  name="symptomName"
                  variant="outlined"
                  inputRef={register()}
                  autoFocus
                  fullWidth
                  sx={{ width: 285 }}
                />
              </Box>
              <Box sx={{ ml: 2 }}>
                <Label for="specialtyId">
                  <Box sx={{ display: "flex" }}>
                    <Typography>Chuyên khoa</Typography>
                    <span className="text-danger">&nbsp;*</span>
                  </Box>
                </Label>
                <Controller
                  name="specialtyId"
                  control={control}
                  render={({ value, onChange }) => (
                    <TextField
                      sx={{ width: 200 }}
                      id="specialtyId"
                      select
                      value={value}
                      onChange={onChange}
                      variant="outlined"
                    >
                      {specialties?.map((specialty) => (
                        <MenuItem
                          key={specialty.specialtyId}
                          value={specialty.specialtyId}
                        >
                          {specialty.specialtyName}
                        </MenuItem>
                      ))}
                    </TextField>
                  )}
                />
              </Box>
            </Box>
            <Box sx={{ mt: 2 }}>
              <Label for="content">
                <Box sx={{ display: "flex" }}>
                  <Typography>Nội dung</Typography>
                  <span className="text-danger">&nbsp;*</span>
                </Box>
              </Label>
              <TextField
                id="content"
                name="content"
                variant="outlined"
                inputRef={register()}
                fullWidth
              />
            </Box>
          </Box>
        </FormGroup>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={handleClose}>
          Huỷ
        </Button>
        <Button
          variant="contained"
          disabled={!formState.isDirty}
          onClick={handleSubmit(onSubmit)}
          type="submit"
        >
          Lưu
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EditSymptomModal;
