import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
  Box,
  Button,
  Card,
  IconButton,
  MenuItem,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Container } from "reactstrap";
import {
  deleteSymptomAPI,
  searchSymptomsAPI,
  setEnabledSymptomAPI,
} from "../../../api/main";
import EditSymptomModal from "./EditSymptomModal";

const MoreOptions = ({
  symptom,
  handleEditModelOpen,
  handleDisabledSymptom,
  handleDeleteSymptom,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      <IconButton
        aria-describedby={id}
        variant="contained"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <MenuItem>
          <Button
            onClick={() => {
              handleEditModelOpen(symptom);
            }}
          >
            Sửa
          </Button>
        </MenuItem>
        <MenuItem>
          {symptom.isEnabled ? (
            <Button
              onClick={() => {
                handleDisabledSymptom(symptom.symptomId, false);
              }}
            >
              Dừng hoạt động
            </Button>
          ) : (
            <Button
              onClick={() => {
                handleDisabledSymptom(symptom.symptomId, true);
              }}
            >
              Kích hoạt
            </Button>
          )}
        </MenuItem>
        <MenuItem>
          <Button onClick={() => handleDeleteSymptom(symptom.symptomId)}>
            Xoá
          </Button>
        </MenuItem>
      </Popover>
    </div>
  );
};

const Symptoms = () => {
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [totalRows, setTotalRows] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [pageNum, setPageNum] = useState(0);
  const [refreshToggle, setRefreshToggle] = useState(false);
  const [symptomEditModalOpen, setSymptomEditModalOpen] = useState(false);
  const [symptom, setSymptom] = useState();

  const fetchData = async (pageSize, pageNumber, keyword) => {
    const res = await searchSymptomsAPI(pageSize, pageNumber, keyword);
    setData(res.data.pageData);
  };

  useEffect(() => {
    fetchData(rowsPerPage, 1, keyword);
  }, [refreshToggle, keyword]);

  const handleSearch = (e) => {
    setKeyword(e.target.value);
  };

  const handlePageChange = (page) => {
    fetchData(rowsPerPage, page, keyword);
  };

  const handleRowPerPageChange = async (newPerPage, page) => {
    fetchData(newPerPage, page, keyword);
    setRowsPerPage(newPerPage);
  };

  const openAddModal = () => {
    setSymptomEditModalOpen(true);
  };

  const handleCloseModal = (result) => {
    setSymptomEditModalOpen(false);
    setSymptom();
    if (result === "Save") {
      setRefreshToggle(!refreshToggle);
    }
  };

  const handleEditModelOpen = (data) => {
    setSymptom(data);
    setSymptomEditModalOpen(true);
  };

  const handleDisabledSymptom = async (symptomId, isEnabled) => {
    const response = await setEnabledSymptomAPI(symptomId, {
      isEnabled: isEnabled,
    });
    console.log(response);
    setRefreshToggle(!refreshToggle);
  };

  const handleDeleteSymptom = async (symptomId) => {
    const response = await deleteSymptomAPI(symptomId);
    console.log(response);
    setRefreshToggle(!refreshToggle);
  };

  return (
    <Container sx={{ backgroundColor: "#F0F2F5" }}>
      <Card
        sx={{
          width: "600px",
          mb: 3,
        }}
      >
        <Box
          sx={{
            p: 2,
            display: "flex",
          }}
        >
          <TextField
            variant="outlined"
            label="Tìm kiếm"
            onChange={handleSearch}
            sx={{ width: 420 }}
          />
          <Button
            variant="contained"
            onClick={() => openAddModal()}
            sx={{ ml: 3 }}
          >
            Thêm mới
          </Button>
        </Box>
      </Card>
      <Card>
        <Table sx={{ minWidth: 650 }}>
          <TableHead>
            <TableRow sx={{ backgroundColor: "#F3F2F7" }}>
              <TableCell sx={{ fontWeight: "600" }} align="center">
                TÊN TRIỆU CHỨNG
              </TableCell>
              <TableCell sx={{ fontWeight: "600" }} align="center">
                CHUYÊN KHOA
              </TableCell>
              <TableCell sx={{ fontWeight: "600" }} align="center">
                NỘI DUNG
              </TableCell>
              <TableCell sx={{ fontWeight: "600" }} align="center"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((row) => (
              <TableRow
                key={row.specialtyId}
                sx={{
                  backgroundColor: row.isEnabled ? "white" : "#EBECF0",
                  opacity: row.isEnabled ? 1 : 0.5,
                }}
              >
                <TableCell align="center">{row.symptomName}</TableCell>
                <TableCell align="center">{row.specialtyName}</TableCell>
                <TableCell align="center">{row.content}</TableCell>
                <TableCell align="center">
                  <MoreOptions
                    symptom={row}
                    handleEditModelOpen={handleEditModelOpen}
                    handleDisabledSymptom={handleDisabledSymptom}
                    handleDeleteSymptom={handleDeleteSymptom}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TablePagination
            rowsPerPageOptions={[5, 10, 15, 20]}
            count={totalRows}
            page={pageNum}
            rowsPerPage={rowsPerPage}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleRowPerPageChange}
          />
        </Table>
      </Card>
      {symptomEditModalOpen && (
        <EditSymptomModal
          symptom={symptom}
          open={symptomEditModalOpen}
          handleClose={handleCloseModal}
        />
      )}
    </Container>
  );
};

export default Symptoms;
