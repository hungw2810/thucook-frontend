import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
  Box,
  Button,
  Card,
  IconButton,
  MenuItem,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Container } from "reactstrap";
import {
  deleteSpecialtyAPI,
  searchSpecialtiesAPI,
  setEnabledSpecialtyAPI,
} from "../../../api/main";
import EditSpecialtyModal from "./EditSpecialtyModal";

const MoreOptions = ({
  specialty,
  handleEditModelOpen,
  handleDisabledSpecialty,
  handleDeleteSpecialty,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      <IconButton
        aria-describedby={id}
        variant="contained"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <MenuItem>
          <Button
            onClick={() => {
              handleEditModelOpen(specialty);
            }}
          >
            Sửa
          </Button>
        </MenuItem>
        <MenuItem>
          {specialty.isEnabled ? (
            <Button
              onClick={() => {
                handleDisabledSpecialty(specialty.specialtyId, false);
              }}
            >
              Dừng hoạt động
            </Button>
          ) : (
            <Button
              onClick={() => {
                handleDisabledSpecialty(specialty.specialtyId, true);
              }}
            >
              Kích hoạt
            </Button>
          )}
        </MenuItem>
        <MenuItem>
          <Button onClick={() => handleDeleteSpecialty(specialty.specialtyId)}>
            Xoá
          </Button>
        </MenuItem>
      </Popover>
    </div>
  );
};

const Specialties = () => {
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [totalRows, setTotalRows] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [pageNum, setPageNum] = useState(0);
  const [refreshToggle, setRefreshToggle] = useState(false);
  const [specialtyEditModalOpen, setSpecialtyEditModalOpen] = useState(false);
  const [specialty, setSpecialty] = useState();

  const fetchData = async (pageSize, pageNumber, keyword) => {
    const res = await searchSpecialtiesAPI(pageSize, pageNumber, keyword);
    setData(res.data.pageData);
  };

  useEffect(() => {
    fetchData(rowsPerPage, 1, keyword);
  }, [refreshToggle, keyword]);

  const handleSearch = (e) => {
    setKeyword(e.target.value);
  };

  const handlePageChange = (page) => {
    fetchData(rowsPerPage, page, keyword);
  };

  const handleRowPerPageChange = async (newPerPage, page) => {
    fetchData(newPerPage, page, keyword);
    setRowsPerPage(newPerPage);
  };

  const openAddModal = () => {
    setSpecialtyEditModalOpen(true);
  };

  const handleCloseModal = (result) => {
    setSpecialtyEditModalOpen(false);
    if (result === "Save") {
      setRefreshToggle(!refreshToggle);
    }
  };

  const handleEditModelOpen = (data) => {
    setSpecialty(data);
    setSpecialtyEditModalOpen(true);
  };

  const handleDisabledSpecialty = async (specialtyId, isEnabled) => {
    const response = await setEnabledSpecialtyAPI(specialtyId, {
      isEnabled: isEnabled,
    });
    console.log(response);
    setRefreshToggle(!refreshToggle);
  };

  const handleDeleteSpecialty = async (specialtyId) => {
    const response = await deleteSpecialtyAPI(specialtyId);
    console.log(response);
    setRefreshToggle(!refreshToggle);
  };

  return (
    <Container sx={{ backgroundColor: "#F0F2F5" }}>
      <Card
        sx={{
          width: "600px",
          mb: 3,
        }}
      >
        <Box
          sx={{
            p: 2,
            display: "flex",
          }}
        >
          <TextField
            variant="outlined"
            label="Tìm kiếm"
            onChange={handleSearch}
            sx={{ width: 420 }}
          />
          <Button
            variant="contained"
            onClick={() => openAddModal()}
            sx={{ ml: 3 }}
          >
            Thêm mới
          </Button>
        </Box>
      </Card>
      <Card>
        <Table sx={{ minWidth: 650 }}>
          <TableHead>
            <TableRow sx={{ backgroundColor: "#F3F2F7" }}>
              <TableCell sx={{ fontWeight: "600" }} align="center">
                TÊN CHUYÊN KHOA
              </TableCell>
              <TableCell sx={{ fontWeight: "600" }} align="center">
                TÊN RÚT GỌN
              </TableCell>
              <TableCell sx={{ fontWeight: "600" }} align="center"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((row) => (
              <TableRow
                key={row.specialtyId}
                sx={{
                  backgroundColor: row.isEnabled ? "white" : "#EBECF0",
                  opacity: row.isEnabled ? 1 : 0.5,
                }}
              >
                <TableCell align="center">{row.specialtyName}</TableCell>
                <TableCell align="center">{row.specialtyShortName}</TableCell>
                <TableCell align="center">
                  <MoreOptions
                    specialty={row}
                    handleEditModelOpen={handleEditModelOpen}
                    handleDisabledSpecialty={handleDisabledSpecialty}
                    handleDeleteSpecialty={handleDeleteSpecialty}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TablePagination
            rowsPerPageOptions={[5, 10, 15, 20]}
            count={totalRows}
            page={pageNum}
            rowsPerPage={rowsPerPage}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleRowPerPageChange}
          />
        </Table>
      </Card>
      {specialtyEditModalOpen && (
        <EditSpecialtyModal
          specialty={specialty}
          open={specialtyEditModalOpen}
          handleClose={handleCloseModal}
        />
      )}
    </Container>
  );
};

export default Specialties;
