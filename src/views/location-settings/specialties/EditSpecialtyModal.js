import { yupResolver } from "@hookform/resolvers/yup";
import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { FormGroup, Label } from "reactstrap";
import * as yup from "yup";
import {
  addSpecialtyAPI,
  getDetailsSpecialtyAPI,
  updateSpecialtyAPI,
} from "../../../api/main";

const EditSpecialtyModal = ({ open, handleClose, specialty }) => {
  const EditSpecialtySchema = yup.object().shape({
    specialtyName: yup.string().required().max(128).trim(),
    specialtyShortName: yup.string().max(128).trim(),
  });

  const defaultValues = {
    specialtyName: "",
    specialtyShortName: "",
  };

  const { register, formState, handleSubmit, errors, reset } = useForm({
    mode: "onChange",
    resolver: yupResolver(EditSpecialtySchema),
    defaultValues,
  });

  useEffect(() => {
    const asyncFn = async () => {
      if (specialty) {
        try {
          const response = await getDetailsSpecialtyAPI(specialty.specialtyId);
          reset({
            ...defaultValues,
            ...{
              specialtyName: response.data.specialtyName,
              specialtyShortName: response.data.specialtyShortName,
            },
          });
        } catch (error) {
          return console.log(">>error", error);
        }
      }
    };
    asyncFn();
  }, [specialty, reset]);

  const onSubmit = async (data) => {
    try {
      if (!!specialty) {
        await updateSpecialtyAPI(specialty.specialtyId, {
          specialtyName: data.specialtyName,
          specialtyShortName: data.specialtyShortName,
        });
      } else {
        await addSpecialtyAPI({
          specialtyName: data.specialtyName,
          specialtyShortName: data.specialtyShortName,
        });
      }
      handleClose("Save");
    } catch (error) {
      return console.log(">>error", error);
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      maxWidth="sm"
      scroll="body"
    >
      <DialogTitle sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">{!!specialty ? "Sửa" : "Thêm mới"}</Typography>
        <IconButton
          sx={{ ml: "auto" }}
          aria-label="close"
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <FormGroup>
          <Box sx={{ pl: 3, pr: 3 }}>
            <Label for="specialtyName">
              <Box sx={{ display: "flex" }}>
                <Typography>Tên chuyên khoa</Typography>
                <span className="text-danger">&nbsp;*</span>
              </Box>
            </Label>
            <TextField
              id="specialtyName"
              name="specialtyName"
              variant="outlined"
              inputRef={register()}
              autoFocus
              fullWidth
            />
            <Label for="specialtyShortName">
              <Box sx={{ display: "flex" }}>
                <Typography>Tên rút gọn</Typography>
              </Box>
            </Label>
            <TextField
              id="specialtyShortName"
              name="specialtyShortName"
              variant="outlined"
              inputRef={register()}
              fullWidth
            />
          </Box>
        </FormGroup>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={handleClose}>
          Huỷ
        </Button>
        <Button
          variant="contained"
          disabled={!formState.isDirty}
          onClick={handleSubmit(onSubmit)}
          type="submit"
        >
          Lưu
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EditSpecialtyModal;
