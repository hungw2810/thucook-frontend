import { Box, Typography } from "@mui/material";
import moment from "moment";
import { GenderDisplayConfig } from "../../../utility/constants";

const PatientInfo = () => {
  const data = {
    fullName: "Nguyễn Ngọc Hưng",
    birthdayUnixTime: 1004227200000,
    gender: 0,
    phoneNumber: "0973360301",
    email: "hungnn@bav.edu.vn",
    address: "Hà Nội",
    heightInCm: 176,
    weightInKg: 74,
  };

  return (
    <Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Tên bệnh nhân
        </Typography>
        <Typography>{data.fullName}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Ngày sinh
        </Typography>
        <Typography>
          {moment(new Date(data.birthdayUnixTime)).format("DD/MM/YYYY")}
        </Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Giới tính
        </Typography>
        <Typography>{GenderDisplayConfig[data.gender].title}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Số điện thoại
        </Typography>
        <Typography>{data.phoneNumber}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Email
        </Typography>
        <Typography>{data.email}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Địa chỉ
        </Typography>
        <Typography>{data.address}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Chiều cao
        </Typography>
        <Typography>{data.heightInCm}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Cân nặng
        </Typography>
        <Typography>{data.weightInKg}</Typography>
      </Box>
    </Box>
  );
};

export default PatientInfo;
