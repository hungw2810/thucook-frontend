import { Box, Button, Tab, Tabs, Typography } from "@mui/material";
import PropTypes from "prop-types";
import { useState } from "react";
import OverView from "./Overview";
import PatientInfo from "./PatientInfo";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Box>{children}</Box>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const RightSideBar = () => {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Box
        sx={{
          m: 2,
          display: "flex",
          justifyContent: "space-between",
          pl: 4,
          pr: 4,
          pt: 3,
        }}
      >
        <Box>
          <Typography variant="h5">Nguyễn Ngọc Hưng</Typography>
          <Typography sx={{ color: "#616161" }}>28/10/2001 - Nam</Typography>
        </Box>
        <Box>
          <Button variant="contained">Tác vụ</Button>
        </Box>
      </Box>
      <Box sx={{ width: 640 }} role="presentation">
        <Tabs
          orientation="horizontal"
          value={value}
          onChange={handleChange}
          sx={{ borderRight: 1, borderColor: "divider" }}
        >
          <Tab label="Tổng quan" {...a11yProps(0)} />
          <Tab label="Thông tin bệnh nhân" {...a11yProps(1)} />
          <Tab label="Lịch sử khám" {...a11yProps(2)} />
        </Tabs>

        <TabPanel value={value} index={0}>
          <OverView />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <PatientInfo />
        </TabPanel>
        <TabPanel value={value} index={2}></TabPanel>
      </Box>
    </>
  );
};
export default RightSideBar;
