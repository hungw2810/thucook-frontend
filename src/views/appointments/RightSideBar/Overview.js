import { Box, Chip, Typography } from "@mui/material";
import { AppointmentStatusDisplayConfig } from "../../../utility/constants";

const OverView = () => {
  const data = {
    appointmentId: 1,
    startDate: "03/01/2022",
    startTime: "15:00",
    doctorName: "Nguyễn Tài Thu",
    symtom: "Đau bụng",
    updatedAt: "31/12/2021 14:15:44",
    appointmentStatusId: 1000,
  };
  return (
    <Box>
      <Box sx={{ mb: 3 }}>
        <Chip
          size="small"
          color={
            AppointmentStatusDisplayConfig[data.appointmentStatusId]?.color
          }
          label={
            AppointmentStatusDisplayConfig[data.appointmentStatusId]?.title
          }
        />
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Ngày hẹn
        </Typography>
        <Typography>{data.startDate}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Giờ hẹn
        </Typography>
        <Typography>{data.startTime}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Tên bác sĩ
        </Typography>
        <Typography>{data.doctorName}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Triệu chứng
        </Typography>
        <Typography>{data.symtom}</Typography>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ color: "#616161" }} variant="caption">
          Cập nhật lúc
        </Typography>
        <Typography>{data.updatedAt}</Typography>
      </Box>
    </Box>
  );
};

export default OverView;
