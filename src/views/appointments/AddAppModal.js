import { yupResolver } from "@hookform/resolvers/yup";
import CloseIcon from "@mui/icons-material/Close";
import {
  Autocomplete,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  TextField,
  Typography,
  MenuItem,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { FormGroup, Label } from "reactstrap";
import * as yup from "yup";
import { getAllSpecialtiesAPI } from "../../api/main";

const AddAppModal = ({ open, handleClose }) => {
  const patients = [
    {
      patientId: "08da4724-12de-4361-823d-b950526e1c7c",
      fullName: "Nguyễn Ngọc Hưng",
    },
    {
      patientId: "08da4725-c8cd-44ed-8acb-8d20504b93e5",
      fullName: "Trần Xuân Chính",
    },
    {
      patientId: "08da4c6b-db83-41de-88bd-258c73a7cfec",
      fullName: "Nguyễn Huy Cương",
    },
  ];
  //const [patients, setPatients] = useState();
  const [specialties, setSpecialties] = useState();

  const AddAppSchema = yup.object().shape({
    patientId: yup.string().required().max(36).trim(),
    specialtyId: yup.string().required().max(36).trim(),
    doctorId: yup.string().required().max(36).trim(),
    scheduleId: yup.number().nullable(),
    startDateTimeUnix: yup.number().required(),
    symtom: yup.string().nullable().max(1024).trim(),
  });

  const defaultValues = {
    patientId: "",
    specialtyId: "",
    doctorId: "",
    scheduleId: null,
    startDateTimeUnix: Math.floor(new Date() / 1000),
    symtom: null,
  };

  const { register, formState, handleSubmit, errors, reset, control } = useForm(
    {
      mode: "onChange",
      resolver: yupResolver(AddAppSchema),
      defaultValues,
    }
  );

  useEffect(() => {
    const asyncFn = async () => {
      const response = await getAllSpecialtiesAPI();
      setSpecialties(response);
      // const response = await getAllPatientsAPI();
      // setPatients(response);
      // console.log(patients);
    };
    asyncFn();
  }, [open, reset]);
  console.log(patients);

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      maxWidth="md"
      scroll="body"
    >
      <DialogTitle sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">Thêm lịch hẹn</Typography>
        <IconButton
          sx={{ ml: "auto" }}
          aria-label="close"
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent sx={{ p: 5 }}>
        <FormGroup>
          <Box display="flex">
            <Box sx={{ mr: 4 }}>
              <Label for="patientId">
                <Box sx={{ display: "flex" }}>
                  <Typography>Hồ sơ bệnh nhân</Typography>
                  <span className="text-danger">&nbsp;*</span>
                </Box>
              </Label>
              <Controller
                render={({ value, onChange }) => (
                  <Autocomplete
                    disablePortal
                    onChange={(e, data) => onChange(data)}
                    value={value}
                    options={patients}
                    getOptionLabel={(option) => option.fullName}
                    sx={{ width: 350 }}
                    renderInput={(params) => <TextField {...params} />}
                  />
                )}
                name="patientId"
                control={control}
              />
            </Box>
            <Box sx={{ mr: 4 }}>
              <Typography>Hoặc</Typography>
              <Button variant="outlined">Thêm</Button>
            </Box>
          </Box>
          <Box sx={{ mr: 4 }}>
            <Label for="specialtyId">
              <Box sx={{ display: "flex" }}>
                <Typography>Chuyên khoa</Typography>
              </Box>
            </Label>
            <Controller
              name="specialtyId"
              control={control}
              render={({ value, onChange }) => (
                <TextField
                  fullWidth
                  id="specialtyId"
                  select
                  value={value}
                  onChange={onChange}
                  variant="outlined"
                  sx={{ width: 350 }}
                >
                  {specialties?.map((specialty) => (
                    <MenuItem
                      key={specialty.specialtyId}
                      value={specialty.specialtyId}
                    >
                      {specialty.specialtyName}
                    </MenuItem>
                  ))}
                </TextField>
              )}
            />
          </Box>
          <Box sx={{ mr: 4 }}>
            <Label for="doctorId">
              <Box sx={{ display: "flex" }}>
                <Typography>Bác sĩ</Typography>
                <span className="text-danger">&nbsp;*</span>
              </Box>
            </Label>
            <TextField
              id="doctorId"
              name="doctorId"
              variant="outlined"
              inputRef={register()}
              sx={{ width: 350 }}
            />
          </Box>
        </FormGroup>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={() => reset(defaultValues)}>
          Reset
        </Button>
        <Button variant="contained">Lưu</Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddAppModal;
