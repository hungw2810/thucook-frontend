import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardContent,
  Chip,
  Drawer,
  Grid,
  MenuItem,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
} from "@mui/material";
import { endOfDay, startOfDay } from "date-fns";
import moment from "moment";
import { useCallback, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { FormGroup } from "reactstrap";
import * as yup from "yup";
import {
  AppointmentStatusDisplayConfig,
  AppointmentStatusEnum,
} from "../../utility/constants";
import { enumToSelectOptions } from "../../utility/utils";
import AddAppModal from "./AddAppModal";
import RightSideBar from "./RightSideBar/RightSideBar";

const Appointments = () => {
  const [filter, setFilter] = useState({
    keyword: "",
    specialtyId: null,
    doctorId: null,
    appointmentStatusId: null,
    fromDate: startOfDay(new Date()).getTime(),
    toDate: endOfDay(new Date()).getTime(),
  });
  const [totalRows, setTotalRows] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [pageNum, setPageNum] = useState(0);
  const [refreshToggle, setRefreshToggle] = useState(false);
  const [openAddAppointmentModal, setOpenAddAppointmentModal] = useState(false);
  const [rightSideBarOpen, setRightSideBarOpen] = useState(false);

  const filterSchema = yup.object().shape({
    keyword: yup.string().max(128),
    patientCode: yup.string(),
    gender: yup.number().nullable(true),
    yearOfBirth: yup.number().nullable(true),
  });

  const defaultValues = {
    keyword: "",
    specialtyId: null,
    doctorId: null,
    appointmentStatusId: null,
    fromDate: startOfDay(new Date()).getTime(),
    toDate: endOfDay(new Date()).getTime(),
  };
  const { register, formState, handleSubmit, errors, reset, control } = useForm(
    {
      mode: "onChange",
      resolver: yupResolver(filterSchema),
      defaultValues,
    }
  );
  const data = [
    {
      startDateTimeUnix: 1641196800000,
      patient: "Nguyễn Ngọc Hưng",
      doctorName: "Nguyễn Tài Thu",
      appointmentStatusId: 1000,
    },
    {
      startDateTimeUnix: 1641278400000,
      patient: "Trần Xuân Chính",
      doctorName: "Nguyễn Văn Thạch",
      appointmentStatusId: 2000,
    },
    {
      startDateTimeUnix: 1641283200000,
      patient: "Nguyễn Huy Cương",
      doctorName: "Nguyễn Tài Thu",
      appointmentStatusId: 9000,
    },
  ];

  const doctor = [
    {
      doctorId: "6d0f465c-1e93-4bbc-8ea4-00482fe92401",
      doctorName: "Nguyễn Tài Thu",
    },
    {
      doctorId: "6d0f465c-1e93-4bbc-8ea4-00482fe92401",
      doctorName: "Nguyễn Văn Thạch",
    },
  ];

  const handleSearch = useCallback(
    (filterData) => {
      const newFilter = { ...filter, ...filterData };
      setFilter(newFilter);
      setPageNum(0);
      setRefreshToggle(!refreshToggle);
      //fetchData(rowsPerPage, 1, newFilter)
    },
    [refreshToggle, rowsPerPage]
  );

  const handleSearchClick = () => {
    setRefreshToggle(!refreshToggle);
  };

  const handlePageChange = (page) => {
    //fetchData(rowsPerPage, page, keyword);
  };

  const handleRowPerPageChange = async (newPerPage, page) => {
    //fetchData(newPerPage, page, keyword);
    setRowsPerPage(newPerPage);
  };

  const handleResetFilter = () => {
    reset(defaultValues);
    handleSearch(defaultValues);
  };

  const toggleSlider = (event) => {
    setRightSideBarOpen(!rightSideBarOpen);
  };

  const handleCloseModal = (result) => {
    setOpenAddAppointmentModal(false);
    if (result === "Save") {
      setRefreshToggle(!refreshToggle);
    }
  };

  return (
    <Box>
      <Grid sx={{ display: "flex" }} container>
        <Grid item md={2}>
          <Card sx={{ pt: 3, pl: 4 }}>
            <CardContent>
              <Button
                sx={{ width: "80%" }}
                variant="contained"
                onClick={() => {
                  setOpenAddAppointmentModal(true);
                }}
              >
                Thêm lịch hẹn
              </Button>
              <Box>
                <FormGroup>
                  <TextField
                    label="Khoảng ngày"
                    variant="outlined"
                    size="small"
                    sx={{ mt: 4 }}
                    name=""
                  ></TextField>
                  <TextField
                    label="Chuyên khoa"
                    variant="outlined"
                    size="small"
                    sx={{ mt: 4 }}
                  ></TextField>
                  <Controller
                    label="Bác sĩ"
                    name="doctorId"
                    control={control}
                    render={({ value, onChange }) => (
                      <TextField
                        sx={{ width: "80%", mt: 4 }}
                        id="doctorId"
                        select
                        value={value}
                        label="Bác sĩ"
                        onChange={onChange}
                        variant="outlined"
                      >
                        {enumToSelectOptions(AppointmentStatusEnum).map(
                          (option) => (
                            <MenuItem key={option.value} value={option.value}>
                              {
                                AppointmentStatusDisplayConfig[option.value]
                                  .title
                              }
                            </MenuItem>
                          )
                        )}
                      </TextField>
                    )}
                  />
                  <Controller
                    lable="Trạng thái"
                    name="appointmentStatusId"
                    control={control}
                    render={({ value, onChange }) => (
                      <TextField
                        sx={{ width: "80%", mt: 4 }}
                        id="appointmentStatusId"
                        select
                        value={value}
                        label="Trạng thái"
                        onChange={onChange}
                        variant="outlined"
                      >
                        {enumToSelectOptions(AppointmentStatusEnum).map(
                          (option) => (
                            <MenuItem key={option.value} value={option.value}>
                              {
                                AppointmentStatusDisplayConfig[option.value]
                                  .title
                              }
                            </MenuItem>
                          )
                        )}
                      </TextField>
                    )}
                  />
                  <TextField
                    variant="outlined"
                    size="small"
                    sx={{ mt: 3 }}
                    label="Từ khoá"
                    inputRef={register()}
                  ></TextField>
                </FormGroup>
                <Box
                  sx={{
                    display: "flex",
                    mt: 3,
                  }}
                >
                  <Button sx={{ mr: 5 }} variant="contained">
                    Tìm
                  </Button>
                  <Button
                    variant="outlined"
                    onClick={() => reset(defaultValues)}
                  >
                    Đặt lại
                  </Button>
                </Box>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item md={10}>
          <TableContainer sx={{ pt: 4, ml: 2, pl: 2 }} component={Paper}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow sx={{ backgroundColor: "#F3F2F7" }}>
                  <TableCell sx={{ fontWeight: "700" }} align="left">
                    NGÀY
                  </TableCell>
                  <TableCell sx={{ fontWeight: "700" }} align="left">
                    BỆNH NHÂN
                  </TableCell>
                  <TableCell sx={{ fontWeight: "700" }} align="left">
                    BÁC SĨ
                  </TableCell>
                  <TableCell sx={{ fontWeight: "700" }} align="left">
                    TRẠNG THÁI
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((row) => (
                  <TableRow
                    key={row.appointmentId}
                    sx={{
                      "&:last-child td, &:last-child th": {
                        border: 0,
                        hover: { cursor: "pointer" },
                      },
                    }}
                    onClick={() => setRightSideBarOpen(true)}
                    hover
                  >
                    <Drawer
                      open={rightSideBarOpen}
                      anchor="right"
                      onClose={toggleSlider}
                    >
                      <RightSideBar />
                    </Drawer>
                    <TableCell align="left">
                      {moment(new Date(row.startDateTimeUnix)).format(
                        "HH:mm - DD/MM/YYYY"
                      )}
                    </TableCell>
                    <TableCell align="left">{row.patient}</TableCell>
                    <TableCell align="left">{row.doctorName}</TableCell>
                    <TableCell align="left">
                      <Chip
                        color={
                          AppointmentStatusDisplayConfig[
                            row.appointmentStatusId
                          ]?.color
                        }
                        label={
                          AppointmentStatusDisplayConfig[
                            row.appointmentStatusId
                          ]?.title
                        }
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              <TablePagination
                rowsPerPageOptions={[5, 10, 15, 20]}
                count={totalRows}
                page={pageNum}
                rowsPerPage={rowsPerPage}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleRowPerPageChange}
              />
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
      {openAddAppointmentModal && (
        <AddAppModal
          open={openAddAppointmentModal}
          handleClose={handleCloseModal}
        />
      )}
    </Box>
  );
};

export default Appointments;
