import axios from "axios";

/**
 * Login
 * @param {{
  userName: string;
  password: string;
}} data
*/

const Axios = axios.create();

const headers = {
  "Content-Type": "application/json",
  Authorization: `Bearer ${localStorage.getItem("token")}`,
};

export const loginAPI = async (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/auth/login`, data);
};

// User apis
export const getUserInfoAPI = async () => {
  return axios
    .get(`${process.env.REACT_APP_API_URL}/users/me`, { headers: headers })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const updateUserInfoAPI = async (data) => {
  return axios
    .put(`${process.env.REACT_APP_API_URL}/users`, data, { headers: headers })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

// Specialty apis
export const searchSpecialtiesAPI = async (
  pageSize,
  pageNumber,
  keyword = ""
) => {
  return axios
    .get(
      `${
        process.env.REACT_APP_API_URL
      }/specialties?pageSize=${pageSize}&pageNumber=${pageNumber}&keyword=${encodeURI(
        keyword
      )}`,
      { headers: headers }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const getAllSpecialtiesAPI = async (pageNumber = 1) => {
  const arr = [];
  const apiRes = await searchSpecialtiesAPI(100, pageNumber, "");
  if (
    apiRes &&
    apiRes.data &&
    apiRes.data.pageData &&
    apiRes.data.pageData.length > 0
  ) {
    arr.push(...apiRes.data.pageData);
    if (
      apiRes.data.paging.totalItem >
      apiRes.data.paging.pageSize + (apiRes.data.paging.pageNumber - 1) * 99
    ) {
      const nexPageRes = await getAllSpecialtiesAPI(pageNumber + 1);
      arr.concat(nexPageRes);
    }
  }
  return arr;
};

export const addSpecialtyAPI = async (data) => {
  return axios
    .post(`${process.env.REACT_APP_API_URL}/specialties`, data, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const setEnabledSpecialtyAPI = async (specialtyId, data) => {
  return axios
    .put(
      `${process.env.REACT_APP_API_URL}/specialties/${specialtyId}/set-enabled`,
      data,
      {
        headers: headers,
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const deleteSpecialtyAPI = async (specialtyId) => {
  return axios
    .delete(`${process.env.REACT_APP_API_URL}/specialties/${specialtyId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const getDetailsSpecialtyAPI = async (specialtyId) => {
  return axios
    .get(`${process.env.REACT_APP_API_URL}/specialties/${specialtyId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const updateSpecialtyAPI = async (specialtyId, details) => {
  return axios
    .put(
      `${process.env.REACT_APP_API_URL}/specialties/${specialtyId}`,
      details,
      {
        headers: headers,
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

// Medicine apis
export const searchMedicinesAPI = async (
  pageSize,
  pageNumber,
  keyword = ""
) => {
  return axios
    .get(
      `${
        process.env.REACT_APP_API_URL
      }/medicines?pageSize=${pageSize}&pageNumber=${pageNumber}&keyword=${encodeURI(
        keyword
      )}`,
      { headers: headers }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const addMedicineAPI = async (data) => {
  return axios
    .post(`${process.env.REACT_APP_API_URL}/medicines`, data, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const getDetailsMedicineAPI = async (medicineId) => {
  return axios
    .get(`${process.env.REACT_APP_API_URL}/medicines/${medicineId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const updateMedicineAPI = async (medicineId, data) => {
  return axios
    .put(`${process.env.REACT_APP_API_URL}/medicines/${medicineId}`, data, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const deleteMedicineAPI = async (medicineId) => {
  return axios
    .delete(`${process.env.REACT_APP_API_URL}/medicines/${medicineId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

// Patient apis
export const createPatientAPI = async (data) => {
  return axios
    .post(`${process.env.REACT_APP_API_URL}/patients`, data, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const searchPatientAPI = async (pageSize, pageNumber, details) => {
  return axios
    .post(
      `${process.env.REACT_APP_API_URL}/patients/search?pageSize=${pageSize}&pageNumber=${pageNumber}`,
      details,
      { headers: headers }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const getDetailsPatientAPI = async (patientId) => {
  return axios
    .get(`${process.env.REACT_APP_API_URL}/patients/${patientId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const updatePatientAPI = async (patientId, details) => {
  return axios
    .put(`${process.env.REACT_APP_API_URL}/patients/${patientId}`, details, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const deletePatientAPI = async (patientId) => {
  return axios
    .delete(`${process.env.REACT_APP_API_URL}/patients/${patientId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const getAllPatientsAPI = async (pageNumber = 1) => {
  const arr = [];
  const details = {
    keyword: "",
    patientCode: "",
    gender: null,
    yearOfBirth: 0,
  };
  const apiRes = await searchPatientAPI(100, pageNumber, details);
  if (
    apiRes &&
    apiRes.data &&
    apiRes.data.pageData &&
    apiRes.data.pageData.length > 0
  ) {
    arr.push(...apiRes.data.pageData);
    if (
      apiRes.data.paging.totalItem >
      apiRes.data.paging.pageSize + (apiRes.data.paging.pageNumber - 1) * 99
    ) {
      const nexPageRes = await getAllPatientsAPI(pageNumber + 1);
      arr.concat(nexPageRes);
    }
  }
  return arr;
};

// Doctor apis
export const searchDoctorsAPI = async (pageSize, pageNumber, keyword = "") => {
  return axios
    .get(
      `${
        process.env.REACT_APP_API_URL
      }/doctors?pageSize=${pageSize}&pageNumber=${pageNumber}&keyword=${encodeURI(
        keyword
      )}`,
      { headers: headers }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const createDoctorAPI = async (data) => {
  return axios
    .post(`${process.env.REACT_APP_API_URL}/doctors`, data, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const setEnabledDoctorAPI = async (doctorId, data) => {
  return axios
    .put(
      `${process.env.REACT_APP_API_URL}/doctors/${doctorId}/set-enabled`,
      data,
      {
        headers: headers,
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const deleteDoctorAPI = async (doctorId) => {
  return axios
    .delete(`${process.env.REACT_APP_API_URL}/doctors/${doctorId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const getDetailsDoctorAPI = async (doctorId) => {
  return axios
    .get(`${process.env.REACT_APP_API_URL}/doctors/${doctorId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const updateDoctorAPI = async (doctorId, details) => {
  return axios
    .put(`${process.env.REACT_APP_API_URL}/doctors/${doctorId}`, details, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

// Symptom aps
export const searchSymptomsAPI = async (pageSize, pageNumber, keyword = "") => {
  return axios
    .get(
      `${
        process.env.REACT_APP_API_URL
      }/symptoms?pageSize=${pageSize}&pageNumber=${pageNumber}&keyword=${encodeURI(
        keyword
      )}`,
      { headers: headers }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const createSymptomAPI = async (data) => {
  return axios
    .post(`${process.env.REACT_APP_API_URL}/symptoms`, data, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const getDetailsSymptomAPI = async (symptomId) => {
  return axios
    .get(`${process.env.REACT_APP_API_URL}/symptoms/${symptomId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const updateSymptomAPI = async (symptomId, data) => {
  return axios
    .put(`${process.env.REACT_APP_API_URL}/symptoms/${symptomId}`, data, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const deleteSymptomAPI = async (symptomId) => {
  return axios
    .delete(`${process.env.REACT_APP_API_URL}/symptoms/${symptomId}`, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};

export const setEnabledSymptomAPI = async (symptomId, data) => {
  return axios
    .put(
      `${process.env.REACT_APP_API_URL}/symptoms/${symptomId}/set-enabled`,
      data,
      {
        headers: headers,
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("error " + error);
    });
};
