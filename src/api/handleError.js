export const getErrorMessage = (error, defaultMessage = 'apiError.unknown') => {
   if (error?.response?.metadata) {
      return error.response.metadata.messages[0].value
   }
   return error.message || defaultMessage
}
export default getErrorMessage
