import AuthLayout from "./layouts/AuthLayout";
import MainLayout from "./layouts/MainLayout";
import Appointments from "./views/appointments/Appointments";
import Login from "./views/auth/Login";
import LocationSettings from "./views/location-settings/LocationSettings";
import Patients from "./views/patients/Patients";
import TodayQueue from "./views/today-queue/TodayQueue";

const routes = [
  {
    path: "/",
    element: <AuthLayout />,
    children: [
      {
        path: "login",
        element: <Login />,
      },
      // {
      //    path: 'register',
      //    element: <Register />
      // }]
    ],
  },
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        path: "appointments",
        element: <Appointments />,
      },
      {
        path: "location-settings",
        element: <LocationSettings />,
      },
      {
        path: "patients",
        element: <Patients />,
      },
      {
        path: "today-queue",
        element: <TodayQueue />,
      },
    ],
  },
];

export default routes;
