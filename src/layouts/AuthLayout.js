import React from "react";
import { Outlet } from "react-router-dom";
import Login from "../views/auth/Login";

const AuthLayout = () => {
  return (
    <>
      <Outlet />
    </>
  );
};

export default AuthLayout;
